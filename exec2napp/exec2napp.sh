#!/bin/bash
if [[ "$1" == "" && "$2" == "" && "$3" == "" ]]
then
    echo "Usage: exec2napp (APP_FOLDER) (METADATA_FILE) (OUTPUT_FILE)"
else
  if [[ -d $1 && -f $2 && "$3" != "" ]]
  then
    echo Creating NebiOS Application from $1 to $3
    	if ! [ `command -v makeself` ]; then
    		echo "=== INSTALLING REQUIRED PACKAGE FOR CREATING APP BUNDLE ==="
     		sudo apt install makeself
     		echo "PLEASE RE-RUN EXEC2NAPP !"
    	fi
    	NAPP_NAME=$(cat $2 | grep " Name=")
	NAPP_NAME="${NAPP_NAME/" Name="/""}"
	NAPP_NAME="${NAPP_NAME/" "/"_"}"
	echo "App name is $NAPP_NAME"
	# Uses makeself for all-in-one application. Puts the metadata (.ninf format looks like desktop launcher file with extra details but it's not same. .ninf will contain more info) to lsm.
	makeself --nomd5 --nocrc --lsm "$2" --notemp --target /Applications/."$NAPP_NAME"/ --nox11 "$1" "$3" "$NAPP_NAME" ./app.exec && echo "Application created successfully. Send to NebiSoft <info@nebisoftware.com> to review and publish to Bundle Store."
  else
    echo Please specify a folder that contains your app and exists!
    echo "Please specify a metadata file (*.ninf) that contains your app info and exists!"
    echo Please give an output file!
  fi
fi
