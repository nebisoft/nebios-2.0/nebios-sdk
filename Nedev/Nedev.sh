#!/bin/bash

# Check napp-runtime version
napp_version=$(napp-container --version)
if [ $? -eq 1 ]; then
	zenity --error --text="Nedev cannot run with the old napp-runtime!\n\nYou might want install newer runtime.\n\nsudo apt install napp-runtime"
	exit 0
fi

# Detect architecture
arch=$(dpkg --print-architecture)

# Select the appropriate file based on architecture
if [ "$arch" = "amd64" ]; then
	pkg_file="data/deps/amd64"
elif [ "$arch" = "arm64" ]; then
	pkg_file="data/deps/arm64"
else
	zenity --error --text="Unsupported architecture: $arch"
	exit 1
fi

# Check if the file exists
if [ ! -f "$pkg_file" ]; then
	zenity --error --text="Package file not found: $pkg_file"
	exit 1
fi

# Find missing packages
missing_pkgs=""
while IFS= read -r pkg; do
	if ! dpkg -s "$pkg" >/dev/null 2>&1; then
		missing_pkgs+="$pkg "
	fi
done < "$pkg_file"

# Exit if there are no missing packages
if [ -z "$missing_pkgs" ]; then
	./Nedev.bin
	exit 0
fi

# Show missing packages info to the user
zenity --info --title="Package Requirements" --text="In order for Nedev IDE to work, the following packages must be installed:\n\nPlease run the following command in your terminal to install them manually:\n\nsudo apt install $(echo $missing_pkgs | sed 's/\n/ /g')"
exit 0
