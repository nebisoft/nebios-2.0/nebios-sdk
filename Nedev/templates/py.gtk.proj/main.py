#!/usr/bin/env python3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MyApp:
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("ui/app.ui")
		self.window:Gtk.Window = self.builder.get_object("win")
		self.window.connect("destroy",lambda _: exit())
		self.window.show_all()

if __name__ == "__main__":
	app = MyApp()
	Gtk.main()
