#!/usr/bin/env python3
import gi, sys, os, cairo, json, shutil
from pathlib import Path  # Import the Path module for path manipulation

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, Pango
from editor import Editor

NEDEV_VERSION = "3.0-dev2024.09.07"

class NewProj:
	def __init__(self, PARENT):
		self.PARENT = PARENT
		self.builder = Gtk.Builder()
		self.builder.add_from_file("./ui/app.ui")
		self.MainWindow: Gtk.Window = self.builder.get_object("new_client")
		self.LangStack:Gtk.Stack = self.builder.get_object("LangStack")
		self.PathBtn:Gtk.FileChooserButton = self.builder.get_object("PathBtn")
		self.PathName:Gtk.Entry = self.builder.get_object("PathName")
		self.CancelBtn:Gtk.Button = self.builder.get_object("newproj_cancel")
		self.OkBtn:Gtk.Button = self.builder.get_object("newproj_create")
		self.OkBtn.connect("clicked", self.CreateProjNow)
		self.MainWindow.show_all()

	def CreateProjNow(self, btn):
		p_lang = self.LangStack.get_visible_child_name()
		p_type_box: Gtk.Box = self.LangStack.get_visible_child()
		p_type_stack: Gtk.Stack = p_type_box.get_children()[2]
		p_type = p_type_stack.get_visible_child_name()
		template = f"{p_lang}.{p_type}.proj"

		p_fpath_parent = self.PathBtn.get_filename()
		p_fpath_name = self.PathName.get_text()
		p_fpath = os.path.join(p_fpath_parent, p_fpath_name)

		# Ensure the destination directory exists or create it
		if not os.path.exists(p_fpath):
			os.makedirs(p_fpath)

		template_directory = os.path.join(os.getcwd(), "templates", template)

		if os.path.exists(template_directory):
			# Copy the contents of the template directory to the destination
			for item in os.listdir(template_directory):
				item_path = os.path.join(template_directory, item)
				if os.path.isfile(item_path):
					shutil.copy(item_path, os.path.join(p_fpath, item))
				elif os.path.isdir(item_path):
					shutil.copytree(item_path, os.path.join(p_fpath, item))
				print("Created", template, "into", p_fpath)
			self.MainWindow.close()
			self.PARENT.OpenProject(p_fpath)
		else:
			print("Template directory not found:", template_directory)
		

class Nedev:
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("./ui/app.ui")
		self.MainWindow: Gtk.Window = self.builder.get_object("welcome_client")
		self.TransHeader: Gtk.HeaderBar = self.builder.get_object("trans_header")
		self.SolidHeader: Gtk.HeaderBar = self.builder.get_object("solid_header")
		self.SolidScrolled: Gtk.ScrolledWindow = self.builder.get_object("solid_scrolled")
		self.RecentBox: Gtk.Box = self.builder.get_object("recent_projects_welcome")
		self.ClientStyleContext: Gtk.StyleContext = self.MainWindow.get_style_context()
	
		self.NedevVersion: Gtk.Label = self.builder.get_object("version_label")
		global NEDEV_VERSION
		self.NedevVersion.set_label("Version:" + NEDEV_VERSION)

		self.MainWindow.set_app_paintable(True)
		self.MainWindow.connect("draw", self.on_draw)
		self.TransHeader.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.0))

		self.NewProj: Gtk.Button = self.builder.get_object("welcome_new_proj")
		self.OpenProj: Gtk.Button = self.builder.get_object("welcome_open_proj")

		self.NewProj.connect("clicked", lambda _: NewProj(self))
		self.OpenProj.connect("clicked", self.on_open_project_clicked)

		self.MainWindow.connect("destroy", lambda _: self.save_recent_projects_and_exit())  # Save recent projects and exit
		self.MainWindow.show_all()
		self.load_recent_projects()  # Load recent projects from the JSON file
		self.update_recent_projects_ui()

	def on_draw(self, widget, context):
		style: Gtk.StyleContext = self.MainWindow.get_style_context()
		bgcolor = style.get_background_color(Gtk.StateFlags.NORMAL)
		bdcolor = style.get_background_color(Gtk.StateFlags.BACKDROP)
		context.set_source_rgba(bgcolor.red, bgcolor.green, bgcolor.blue, 0.75)
		context.set_operator(cairo.OPERATOR_SOURCE)
		context.paint()
		context.set_operator(cairo.OPERATOR_OVER)

		self.SolidHeader.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(bdcolor.red, bdcolor.green, bdcolor.blue, 1))
		self.SolidScrolled.get_child().override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(bdcolor.red, bdcolor.green, bdcolor.blue, 1))

	def on_open_project_clicked(self, button):
		dialog = Gtk.FileChooserDialog(
			"Open Project",
			self.MainWindow,
			Gtk.FileChooserAction.SELECT_FOLDER,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK),
		)

		dialog.set_default_response(Gtk.ResponseType.OK)

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			folder_path = dialog.get_filename()
			dialog.destroy()
			self.recent_projects.append(folder_path)
			self.OpenProject(folder_path)

		else:
			dialog.destroy()

	def load_recent_projects(self):
		config_dir = Path.home() / ".config/NebiSoft"
		config_dir.mkdir(parents=True, exist_ok=True)
		recent_file = config_dir / "Nedev.Recents.json"

		if recent_file.is_file():
			with recent_file.open("r") as file:
				self.recent_projects = json.load(file)
		else:
			self.recent_projects= []

	def save_recent_projects(self):
		config_dir = Path.home() / ".config/NebiSoft"
		config_dir.mkdir(parents=True, exist_ok=True)
		recent_file = config_dir / "Nedev.Recents.json"

		with recent_file.open("w") as file:
			json.dump(self.recent_projects, file)

	def save_recent_projects_and_exit(self):
		self.save_recent_projects()
		Gtk.main_quit()

	def update_recent_projects_ui(self):
		for project_path in self.recent_projects:
			box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
			box.set_margin_top(8)
			box.set_margin_bottom(8)
			NameLabel = Gtk.Label()
			NameLabel.set_markup(f"<b>{os.path.basename(project_path)}</b>")
			NameLabel.set_ellipsize(Pango.EllipsizeMode.END)
			NameLabel.set_xalign(0.0)
			PathLabel = Gtk.Label(project_path)
			PathLabel.set_ellipsize(Pango.EllipsizeMode.END)
			PathLabel.set_xalign(0.0)
			box.add(NameLabel)
			box.add(PathLabel)
			button = Gtk.Button()
			button.add(box)
			button.connect("clicked", self.on_recent_project_button_clicked, project_path)
			self.RecentBox.pack_start(button, False, False, 0)
			button.show_all()

	def on_recent_project_button_clicked(self, button, project_path):
		self.OpenProject(project_path)

	def OpenProject(self, fp):
		self.MainWindow.hide()
		self.save_recent_projects()
		Editor(fp, self)

if __name__ == "__main__":
	if len(sys.argv) > 1:
		args = sys.argv[1:]
		print(args)
		for directory in args:
			fpath = os.path.expanduser(directory)
			if fpath.endswith("/"):
				fpath = fpath[:-1]
			Editor(fpath, None)
	else:
		Nedev()
	
	Gtk.main()

