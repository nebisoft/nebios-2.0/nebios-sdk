#!/usr/bin/env python3
import os, sys, subprocess, gi, re, jedi, ast, threading

gi.require_version("Gtk", "3.0")
gi.require_version("GtkSource", "3.0")
gi.require_version('Vte', '2.91')
from gi.repository import Gtk, GtkSource, Pango, Vte, GdkPixbuf, WebKit2, Gio, Gdk, GObject, GLib, Pango

CAMBALACHE_EDITOR_PATH = "\"" + os.getcwd() + "/tools/uiEditor/app.exec\""

SUPPORTED_FILES_EDITOR = [
	".sh",
	".py",
	".h",
	".c",
	".cpp",
	".swift",
	".lua",
	".htm",
	".html",
	".css",
	".js",
	".json",
	".build",
	".ninja",
	".php",
	".gpl",
	".ninf",
	".txt",
	".md"
	"Makefile",
	"amd64",
	"arm64"
]

SUPPORTED_FILES_MIXED = {
	".cmb"		:   CAMBALACHE_EDITOR_PATH,
	".ui"		:   "glade",
	".glade"	:   "glade",
	""		:   "gio open"
}

EDITOR_TREE_BLACKLIST = [
	"project_props",
	"output_pkg"
]

from jedi.api import Script
from jedi.api.environment import Environment

class SearchReplace(Gtk.Box):
	def __init__(self, text_buffer, source_view):
		Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
		self.set_border_width(10)
		self.text_buffer = text_buffer
		self.source_view = source_view

		self.search_entry = Gtk.Entry()
		self.replace_entry = Gtk.Entry()
		self.found = 0

		self.case_sensitive_checkbox = Gtk.CheckButton("Match Case")

		search_button = Gtk.Button()
		replace_button = Gtk.Button()
		replace_all_button = Gtk.Button(label="All")
		destroy_button = Gtk.Button()

		search_button.set_image(Gtk.Image.new_from_icon_name("edit-find-symbolic", Gtk.IconSize.BUTTON))
		replace_button.set_image(Gtk.Image.new_from_icon_name("edit-find-replace-symbolic", Gtk.IconSize.BUTTON))
		replace_all_button.set_image(Gtk.Image.new_from_icon_name("edit-find-replace-symbolic", Gtk.IconSize.BUTTON))
		destroy_button.set_image(Gtk.Image.new_from_icon_name("close-symbolic", Gtk.IconSize.BUTTON))

		search_button.connect("clicked", self.on_search_clicked)
		replace_button.connect("clicked", self.on_replace_clicked)
		replace_all_button.connect("clicked", self.on_replace_all_clicked)
		destroy_button.connect("clicked", lambda _: self.hide())

		self.search_entry.connect("changed", self.on_search_entry_changed)
		self.search_entry.connect("activate", self.on_search_entry_activated)

		self.search_entry.set_placeholder_text("Search Query")
		self.replace_entry.set_placeholder_text("Replace Query")

		self.search_entry.set_hexpand(True)
		self.replace_entry.set_hexpand(True)

		self.add(self.search_entry)
		self.add(self.replace_entry)
		self.add(self.case_sensitive_checkbox)
		self.add(search_button)
		self.add(replace_button)
		self.add(replace_all_button)
		self.add(destroy_button)

	def on_search_entry_changed(self, entry):
		self.found = 0

	def on_search_clicked(self, button=None):
		text_to_find = self.search_entry.get_text()
		start_iter = self.text_buffer.get_iter_at_mark(
			self.text_buffer.get_selection_bound())
		end_iter = self.text_buffer.get_end_iter()
		case_sensitive = self.case_sensitive_checkbox.get_active()
		search_flags = Gtk.TextSearchFlags.CASE_INSENSITIVE if not case_sensitive else 0

		try:
			match_start, match_end = start_iter.forward_search(text_to_find, search_flags, end_iter)
			if match_start is not None:
				self.text_buffer.select_range(match_start, match_end)
				# Scroll to the found text
				self.source_view.scroll_to_iter(match_start, 0.0, use_align=True, xalign=0.5, yalign=0.5)
				self.found += 1
		except:
			# If no matches found, return to the start of the document
			start_iter = self.text_buffer.get_start_iter()
			self.text_buffer.place_cursor(start_iter)

	def on_search_entry_activated(self, entry):
		# Get the text from the Gtk Entry
		text_to_find = entry.get_text()

		# Check if the text is not empty
		if text_to_find:
			self.on_search_clicked(None)

	def on_replace_clicked(self, button):
		bounds = self.text_buffer.get_selection_bounds()

		if bounds:
			selected_start_iter, selected_end_iter = bounds
			selected_text = self.text_buffer.get_text(selected_start_iter, selected_end_iter, False)
			replacement_text = self.replace_entry.get_text()

			if selected_text:
				self.text_buffer.begin_user_action()
				self.text_buffer.delete(selected_start_iter, selected_end_iter)
				self.text_buffer.insert(selected_start_iter, replacement_text)
				self.text_buffer.end_user_action()
				self.on_search_clicked(button)

	def on_replace_all_clicked(self, button):
		text_to_find = self.search_entry.get_text()
		text_to_replace = self.replace_entry.get_text()
		start_iter = self.text_buffer.get_start_iter()
		end_iter = self.text_buffer.get_end_iter()
		self.text_buffer.begin_user_action()

		while start_iter.forward_search(text_to_find, 0, end_iter) is not None:
			match_start, match_end = start_iter.forward_search(text_to_find, 0, end_iter)
			self.text_buffer.delete(match_start, match_end)
			self.text_buffer.insert(match_start, text_to_replace)
			start_iter = match_start

		self.text_buffer.end_user_action()

class Jedi: 
	def get_script(document, extra_paths=None):
		language_id = document.get_language().get_id()
		if language_id.startswith("python"):
			doc_text = document.get_text(document.get_start_iter(), document.get_end_iter(), False)
			iter_cursor = document.get_iter_at_mark(document.get_insert())
			linenum = iter_cursor.get_line() + 1
			charnum = iter_cursor.get_line_index()

			# Create a Jedi Project and add extra paths if provided
			project = jedi.Project('.', added_sys_path=extra_paths if extra_paths else [])

			# Create the Script object with the project
			script = jedi.Script(doc_text, project=project)

			return script
		else:
			return None
		
class JediCompletionProvider(GObject.Object, GtkSource.CompletionProvider):
	__gtype_name__ = 'GediProvider'

	def __init__(self, extra_paths=None):
		GObject.Object.__init__(self)
		self.extra_paths = extra_paths if extra_paths else []

	def do_get_name(self):
		return "Jedi Python Code Completion"
	
	def get_iter_correctly(self, context):
		if isinstance(context.get_iter(), tuple):
			return context.get_iter()[1]
		else:
			return context.get_iter()

	def do_match(self, context):
		iter = self.get_iter_correctly(context)
		iter.backward_char()
		buffer = iter.get_buffer()
		if buffer.get_context_classes_at_iter(iter) != ['no-spell-check']:
			return False
		ch = iter.get_char()
		if not (ch in ('_', '.') or ch.isalnum()):
			return False

		return True

	def do_get_priority(self):
		return 1

	def do_get_activation(self):
		return GtkSource.CompletionActivation.INTERACTIVE

	def do_populate(self, context):
		it = self.get_iter_correctly(context)
		document = it.get_buffer()
		line_start_iter = it.copy()
		line_start_iter.set_line_offset(0)  # Set the iterator to the start of the line
		line_text = line_start_iter.get_text(it)  # Get the text from the start of the line to the cursor iterator
		proposals = []

		# Pass the extra paths to the Jedi script creation
		script = Jedi.get_script(document, extra_paths=self.extra_paths)
		if script is not None:
			for completion in script.complete(line_start_iter.get_line() + 1, len(line_text)):
				proposal = GtkSource.CompletionItem(
					label=completion.name,
					text=completion.name,
					icon=None,
					info=None
				)
				proposals.append(proposal)

		context.add_proposals(self, proposals, True)

class Editor:
	def __init__(self, project_path:str, parent=None):
		self.project_dir = project_path		
		self.PARENT = parent
		self.returns_to_main_menu = False
		self.syntax_check_timer = None

		UiBuilder = Gtk.Builder()
		UiBuilder.add_from_file("ui/app.ui")

		self.editor_window = UiBuilder.get_object("editor")
		self.editor_window.connect("destroy", lambda _: self.QuitEditor())

		if not os.path.exists(self.project_dir):
			self.returns_to_main_menu = True
			self.CloseProject()

		self.editor_header = UiBuilder.get_object("editor_header")
		dirname = self.project_dir.rsplit("/", 1)
		print(dirname)
		if dirname == ["."]:
			dirname = ["sh", "Shell Current Dir"]
		self.editor_window.set_title(dirname[1] + " - Nedev")
		self.editor_header.set_subtitle(self.project_dir.replace(os.getenv("HOME"), "~"))
		
		self.accel_group = UiBuilder.get_object("accel_group")
		
		# === MENU BAR ===
		
		# --- File ---
		
		self.new_button = UiBuilder.get_object("new_item")
		self.save_button = UiBuilder.get_object("save_item")
		self.save_as_button = UiBuilder.get_object("save_as_item")
		# ------
		self.close_proj_button = UiBuilder.get_object("close_proj_item")
		self.quit_button = UiBuilder.get_object("quit_item")
		
		# --- Editor ---
		
		self.cut_button = UiBuilder.get_object("cut_item")
		self.copy_button = UiBuilder.get_object("copy_item")
		self.paste_button = UiBuilder.get_object("paste_item")
		# ------
		self.select_all_button = UiBuilder.get_object("select_all_item")
		self.find_button = UiBuilder.get_object("find_item")
		# ------
		self.close_tab_button = UiBuilder.get_object("close_tab_item")
		
		# --- Tools ---
		
		self.cmb_button = UiBuilder.get_object("cmb_item")
		self.crosinset_button = UiBuilder.get_object("qemu_item")
		# ------
		self.term_new_button = UiBuilder.get_object("term_new_item")
		self.term_close_button = UiBuilder.get_object("term_close_item")
		# ------
		self.term_copy_button = UiBuilder.get_object("term_copy_item")
		self.term_paste_button = UiBuilder.get_object("term_paste_item")
		
		# --- Project ---
		
		self.build_button = UiBuilder.get_object("build_item")
		self.run_button = UiBuilder.get_object("run_item")
		self.debug_button = UiBuilder.get_object("debug_item")
		# ------
		self.maintainhub_button = UiBuilder.get_object("maintainhub_item")
		
		# === KEYBINDS ===

		# --- File ---
		new_key, new_modifier = Gdk.keyval_from_name("N"), Gdk.ModifierType.CONTROL_MASK
		self.new_button.add_accelerator("activate", self.accel_group, new_key, new_modifier, Gtk.AccelFlags.VISIBLE)

		save_key, save_modifier = Gdk.keyval_from_name("S"), Gdk.ModifierType.CONTROL_MASK
		self.save_button.add_accelerator("activate", self.accel_group, save_key, save_modifier, Gtk.AccelFlags.VISIBLE)

		save_as_key, save_as_modifier = Gdk.keyval_from_name("S"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.save_as_button.add_accelerator("activate", self.accel_group, save_as_key, save_as_modifier, Gtk.AccelFlags.VISIBLE)

		close_proj_key, close_proj_modifier = Gdk.keyval_from_name("F4"), Gdk.ModifierType.MOD1_MASK | Gdk.ModifierType.SHIFT_MASK
		self.close_proj_button.add_accelerator("activate", self.accel_group, close_proj_key, close_proj_modifier, Gtk.AccelFlags.VISIBLE)

		quit_key, quit_modifier = Gdk.keyval_from_name("Q"), Gdk.ModifierType.CONTROL_MASK
		self.quit_button.add_accelerator("activate", self.accel_group, quit_key, quit_modifier, Gtk.AccelFlags.VISIBLE)

		# --- Editor ---
		find_key, find_modifier = Gdk.keyval_from_name("F"), Gdk.ModifierType.CONTROL_MASK
		self.find_button.add_accelerator("activate", self.accel_group, find_key, find_modifier, Gtk.AccelFlags.VISIBLE)

		close_tab_key, close_tab_modifier = Gdk.keyval_from_name("W"), Gdk.ModifierType.CONTROL_MASK
		self.close_tab_button.add_accelerator("activate", self.accel_group, close_tab_key, close_tab_modifier, Gtk.AccelFlags.VISIBLE)

		# --- Tools ---
		term_new_key, term_new_modifier = Gdk.keyval_from_name("T"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.term_new_button.add_accelerator("activate", self.accel_group, term_new_key, term_new_modifier, Gtk.AccelFlags.VISIBLE)

		term_close_key, term_close_modifier = Gdk.keyval_from_name("W"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.term_close_button.add_accelerator("activate", self.accel_group, term_close_key, term_close_modifier, Gtk.AccelFlags.VISIBLE)
		
		copy_key, copy_modifier = Gdk.keyval_from_name("C"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.term_copy_button.add_accelerator("activate", self.accel_group, copy_key, copy_modifier, Gtk.AccelFlags.VISIBLE)

		paste_key, paste_modifier = Gdk.keyval_from_name("V"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.term_paste_button.add_accelerator("activate", self.accel_group, paste_key, paste_modifier, Gtk.AccelFlags.VISIBLE)

		# --- Project ---
		build_key, build_modifier = Gdk.keyval_from_name("B"), Gdk.ModifierType.CONTROL_MASK
		self.build_button.add_accelerator("activate", self.accel_group, build_key, build_modifier, Gtk.AccelFlags.VISIBLE)

		run_key, run_modifier = Gdk.keyval_from_name("R"), Gdk.ModifierType.CONTROL_MASK
		self.run_button.add_accelerator("activate", self.accel_group, run_key, run_modifier, Gtk.AccelFlags.VISIBLE)

		debug_key, debug_modifier = Gdk.keyval_from_name("D"), Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK
		self.debug_button.add_accelerator("activate", self.accel_group, debug_key, debug_modifier, Gtk.AccelFlags.VISIBLE)
		
		# === SINGALS ===
		
		# --- File ---
		
		self.new_button.connect("activate", lambda _: self.NewFile())
		self.save_button.connect("activate", lambda _: self.SaveFile())
		self.save_as_button.connect("activate", lambda _: self.SaveFileAs())
		# ------
		self.close_proj_button.connect("activate", lambda _: self.CloseProject())
		self.quit_button.connect("activate", lambda _: exit(0))
		
		# --- Editor ---
		
		self.cut_button.connect("activate", lambda _: self.CutText())
		self.copy_button.connect("activate", lambda _: self.CopyText())
		self.paste_button.connect("activate", lambda _: self.PasteText())
		# ------
		self.select_all_button.connect("activate", lambda _: self.SelectAllText())
		self.find_button.connect("activate", lambda _: self.FindInFile())
		# ------
		self.close_tab_button.connect("activate", lambda _: self.CloseTab())
		
		# --- Tools ---
		
		self.term_new_button.connect("activate", lambda _: self.NewEditorTerminal())  # New terminal
		self.term_close_button.connect("activate", lambda _: self.CloseEditorTerminal())  # Close terminal
		# ------
		self.term_copy_button.connect("activate", lambda _: self.CopyTerminalText())  # Copy text in terminal
		self.term_paste_button.connect("activate", lambda _: self.PasteTerminalText())  # Paste text in terminal
		
		# --- Project ---
		
		self.build_button.connect("activate", lambda _: self.SaveAndRunTarget("Build", "make"))
		self.run_button.connect("activate", lambda _: self.SaveAndRunTarget("Run", "make run"))
		self.debug_button.connect("activate", lambda _: self.SaveAndRunTarget("Debug", "make debug"))
		
		# === PANELS ===

		# Left Panels
		self.project_tree = UiBuilder.get_object("project_tree")
		self.project_warnings = UiBuilder.get_object("project_warnings")

		# Right Panels
		self.editor_container = UiBuilder.get_object("editor_container")
		self.build_debug_terminals = UiBuilder.get_object("build_debug_terminals")

		# Init project tree
		self.project_tree_model = Gtk.TreeStore(str)
		self.project_tree_widget = Gtk.TreeView(self.project_tree_model)
		tvcolumn = Gtk.TreeViewColumn(self.project_dir)
		self.project_tree_widget.append_column(tvcolumn)
		cell = Gtk.CellRendererText()
		tvcolumn.pack_start(cell,True)
		tvcolumn.add_attribute(cell,'text',0)
		self.project_tree.pack_start(self.project_tree_widget, True, True, 0)
		self.project_tree_widget.connect("row-activated", self.ProjectTreeToEditor)
		self.project_tree.connect("button-release-event", self.on_tree_view_button_release)

		self.RefreshProjectTree()

		# Init Command Line Inline Windows
		self.TerminalCounter = 0
		self.editor_vte = Gtk.Notebook()
		self.editor_vte.set_action_widget(UiBuilder.get_object("add_terminal"), Gtk.PackType.END)
		self.editor_vte.set_scrollable(True)
		self.editor_vte.set_show_border(False)
		UiBuilder.get_object("add_terminal").connect("clicked", lambda _: self.NewEditorTerminal())
		UiBuilder.get_object("build_app").connect("clicked", lambda _: self.SaveAndRunTarget("Build", "make"))
		UiBuilder.get_object("run_app").connect("clicked", lambda _: self.SaveAndRunTarget("Run", "make run"))
		UiBuilder.get_object("dbg_app").connect("clicked", lambda _: self.SaveAndRunTarget("Debug", "make debug"))
		self.build_debug_terminals.pack_start(self.editor_vte, True, True, 1)
		self.NewEditorTerminal()

		# Init GtkSourceView
		self.editor_tabs = Gtk.Notebook()
		self.editor_tabs.set_show_border(False)
		self.editor_tabs.set_scrollable(True)
		self.editor_container.pack_start(self.editor_tabs, True, True, 2)
		
		css_provider = Gtk.CssProvider()
		css_provider.load_from_file(Gio.File.new_for_path("./ui/app.css"))
		context = Gtk.StyleContext()
		screen = Gdk.Screen.get_default()
		context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


		self.editor_window.show_all()
		
	def CloseEditorTerminal(self):
		"""Mevcut terminal sekmesini kapat."""
		current_page = self.editor_vte.get_current_page()
		if current_page != -1:
			self.editor_vte.remove_page(current_page)

	def CopyTerminalText(self):
		"""Mevcut terminalde seçilen metni kopyalar."""
		terminal = self.GetTabVte(self.editor_vte.get_current_page())
		if terminal and isinstance(terminal, Vte.Terminal):
			terminal.copy_clipboard()

	def PasteTerminalText(self):
		"""Mevcut terminale yapıştırır."""
		terminal = self.GetTabVte(self.editor_vte.get_current_page())
		if terminal and isinstance(terminal, Vte.Terminal):
			terminal.paste_clipboard()
		
	def CloseTab(self):
		# Mevcut sekmeyi al
		current_page = self.editor_tabs.get_current_page()

		# Eğer sekme varsa, kapat
		if current_page != -1:
			self.editor_tabs.remove_page(current_page)
		
	def CutText(self):
		source_view = self.GetTabSourceView(self.editor_tabs.get_current_page())
		if source_view:
			source_view.get_buffer().cut_clipboard(Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), True)

	def CopyText(self):
		source_view = self.GetTabSourceView(self.editor_tabs.get_current_page())
		if source_view:
			source_view.get_buffer().copy_clipboard(Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD))

	def PasteText(self):
		source_view = self.GetTabSourceView(self.editor_tabs.get_current_page())
		if source_view:
			source_view.get_buffer().paste_clipboard(Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), None, True)

	def SelectAllText(self):
		source_view = self.GetTabSourceView(self.editor_tabs.get_current_page())
		if source_view:
			buffer = source_view.get_buffer()
			buffer.select_range(buffer.get_start_iter(), buffer.get_end_iter())
		
	def SaveExpandedPaths(self):
		expanded_paths = []
		def foreach_function(model, path, iter, expanded_paths):
			if self.project_tree_widget.row_expanded(path):
				expanded_paths.append(path.to_string())
		self.project_tree_model.foreach(foreach_function, expanded_paths)

		# Save the paths to a file or to the application settings
		with open(self.project_dir + "/.expanded_paths.txt", "w") as file:
			for path in expanded_paths:
				file.write(f"{path}\n")
				
	def RestoreExpandedPaths(self):
		try:
			with open(self.project_dir + "/.expanded_paths.txt", "r") as file:
				for line in file.readlines():
					path = Gtk.TreePath.new_from_string(line.strip())
					self.project_tree_widget.expand_row(path, False)
		except FileNotFoundError:
			# If the file does not exist, no paths were saved
			pass

	def SaveAndRunTarget(self, name, cmd):
		try:
			self.SaveFile()
		except:
			print("Error when saving file")

		self.NewEditorTerminal(name, ["bash", "-c", "echo Running app at $PWD && " + cmd])

	def on_tree_view_button_release(self, tree_view: Gtk.TreeView, event):
		if event.button == 3:  # Right mouse button
			# Get the path of the selected item
			selection = self.project_tree_widget.get_selection()
			model, selected_iter = selection.get_selected()
			if selected_iter:
				# Get the path of the selected item
				path = model.get_path(selected_iter).to_string()
				print(path)
				real_path = self.project_dir_path_dict.get(path)
				print(real_path)

				menu = Gtk.Menu()

				# "Open" menu item
				open_item = Gtk.MenuItem(label="Open in Files")
				open_item.connect("activate", lambda _: self.open_in_file_manager(real_path))
				menu.append(open_item)

				# "Rename" menu item
				rename_item = Gtk.MenuItem(label="Rename")
				rename_item.connect("activate", lambda _: self.rename_selected_item(real_path))
				menu.append(rename_item)

				# "Delete" menu item
				delete_item = Gtk.MenuItem(label="Delete")
				delete_item.connect("activate", lambda _: self.delete_selected_item(real_path))
				menu.append(delete_item)

				menu.show_all()
				menu.popup(None, None, None, None, event.button, event.time)

		return True

	def delete_selected_item(self, item_path):
		dialog = Gtk.MessageDialog(parent=self.editor_window, flags=0, message_type=Gtk.MessageType.QUESTION,
								   buttons=Gtk.ButtonsType.YES_NO, text="Confirm Deletion")
		dialog.format_secondary_text(f"Are you sure you want to delete '{item_path}'?")
		response = dialog.run()
		if response == Gtk.ResponseType.YES:
			if os.path.isdir(item_path):
				os.rmdir(item_path)
			else:
				os.remove(item_path)
			self.RefreshProjectTree()
			print(f"Deleted: {item_path}")
		dialog.destroy()

	def open_in_file_manager(self, item_path):
		# Use Nautilus to open the selected item
		subprocess.Popen(["nautilus", item_path])

	def rename_selected_item(self, item_path):
		dialog = Gtk.Dialog(title="Rename Item", parent=self.editor_window, flags=0)
		dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)

		# Create entry box for new name
		entry = Gtk.Entry()
		entry.set_text(os.path.basename(item_path))  # Set current name as default
		box = dialog.get_content_area()
		box.add(Gtk.Label(label="New name:"))
		box.add(entry)
		dialog.show_all()

		# Get user response
		response = dialog.run()
		new_name = entry.get_text().strip()

		if response == Gtk.ResponseType.OK and new_name:
			new_item_path = os.path.join(os.path.dirname(item_path), new_name)
			try:
				os.rename(item_path, new_item_path)
				print(f"Renamed: {item_path} to {new_item_path}")
				self.SaveExpandedPaths()
				self.RefreshProjectTree()  # Refresh the project tree to reflect changes
			except Exception as e:
				error_dialog = Gtk.MessageDialog(
					parent=self.editor_window,
					flags=0,
					message_type=Gtk.MessageType.ERROR,
					buttons=Gtk.ButtonsType.CLOSE,
					text="Error Renaming",
				)
				error_dialog.format_secondary_text(str(e))
				error_dialog.run()
				error_dialog.destroy()

		dialog.destroy()

		
	def ProjectTreeToEditor(self, tree_view, path, column):
		file_path = self.project_dir_path_dict.get(path.to_string())
		
		if not file_path:
			print("File path not found for:", path.to_string())
			print("All files:", self.project_dir_path_dict)
			return

		key_list = list(self.project_dir_path_dict.keys())
		val_list = list(self.project_dir_path_dict.values())
		print("Selected path:", path.to_string())

		try:
			position = val_list.index(file_path)
			corresponding_key = key_list[position]
			print("Corresponding key:", corresponding_key)
		except ValueError:
			print("File path not found in dictionary.")
			return

		print("File path:", file_path)

		try:
			file_ext = "." + str(file_path).rsplit("/", 1)[-1].rsplit(".", 1)[1].lower()
		except IndexError:
			file_ext = str(file_path).rsplit("/", 1)[-1]
		
		image_formats = [format.name.lower() for format in GdkPixbuf.Pixbuf.get_formats()]
		print(image_formats)
		
		if file_ext in SUPPORTED_FILES_EDITOR:
			print("Opening file", file_path, "in editor")
			self.NewEditorTab(file_path)
		elif file_ext[1:] in image_formats:
			print("Opening image file", file_path, "in editor")
			self.NewEditorImageTab(file_path)
		elif file_ext in SUPPORTED_FILES_MIXED.keys():
			print("Opening file", file_path, "- waiting for response")
			dialog = Gtk.MessageDialog(parent=self.editor_window, flags=0, message_type=Gtk.MessageType.QUESTION,
				buttons=Gtk.ButtonsType.YES_NO, text="Open in external editor?")
			dialog.format_secondary_text("You can open this file both on Nedev and GUI editor.")
			editor_name = SUPPORTED_FILES_MIXED.get(file_ext)
			result = dialog.run()
			if result == Gtk.ResponseType.YES:
				print("Opening in external editor:", editor_name)
				os.system(f"{editor_name} \"{file_path}\" &")
			elif result == Gtk.ResponseType.NO:
				print("Opening in Nedev IDE...")
				self.NewEditorTab(file_path)
			dialog.destroy()
		else:
			print("Unsupported file format:", file_ext)
			
	def FindInFile(self):
		self.editor_tabs.get_nth_page(self.editor_tabs.get_current_page()).get_children()[-1].get_children()[0].show()
		
	def GetTabSourceView(self, tab_index):
		content_page = self.editor_tabs.get_nth_page(tab_index).get_children()[0]
		if isinstance(content_page, Gtk.Paned) == True:
			content_view = content_page.get_child1().get_children()[0]
		elif isinstance(content_page, Gtk.ScrolledWindow) == True:
			content_view = content_page.get_children()[0]
		else:
			return None
		return content_view
		
	def GetTabVte(self, tab_index):
		content_view = self.editor_vte.get_nth_page(tab_index).get_children()[0]
		return content_view
				
	def SaveFile(self):
		save_path = self.project_dir + "/" + self.editor_tabs.get_tab_label(self.editor_tabs.get_nth_page(self.editor_tabs.get_current_page())).get_center_widget().get_text()
		print("Saving to: ", save_path)
		content_page = self.editor_tabs.get_nth_page(self.editor_tabs.get_current_page()).get_children()[0]
		content_view = self.GetTabSourceView(self.editor_tabs.get_current_page())
		content = content_view.get_buffer().get_text(content_view.get_buffer().get_start_iter(), content_view.get_buffer().get_end_iter(), True)
		with open(save_path, "w", encoding="utf8") as proj_file:
			proj_file.write(content)
			proj_file.close()
		if isinstance(content_page, Gtk.Paned) == True:
			webview = content_page.get_child2()
			print(webview)
			webview.reload()
			
	def clear_warnings(self):
		for child in self.project_warnings.get_children():
			self.project_warnings.remove(child)

	def test_syntax(self, path, page):
		def run_pylint():
			result = subprocess.run(["pylint", path], capture_output=True, text=True)
			warnings = result.stdout.splitlines()
			# Warnings'i GTK arayüzüne eklemek için ana iş parçacığına (main thread) geç
			GLib.idle_add(self.display_warnings, warnings, page)
		buffer = self.GetTabSourceView(page).get_buffer()
		if buffer.get_language().get_name().lower() == "python":
			thread = threading.Thread(target=run_pylint)
			thread.start()


	def display_warnings(self, warnings, page):
		for child in self.project_warnings.get_children():
			self.project_warnings.remove(child)
			
		for warning in warnings:
			print(warning)
			button = Gtk.Button()
			label = Gtk.Label()
			button.add(label)
			label.set_text(warning)
			label.set_ellipsize(Pango.EllipsizeMode.START)
			try:
				line_number = int(warning.split(":")[1].strip())
				button.connect("clicked", self.on_warning_clicked, page, line_number)
				self.project_warnings.pack_start(button, False, False, 0)
			except:
				pass

		self.project_warnings.show_all()
		
	def on_warning_clicked(self, button, page, line_number):
		self.editor_tabs.set_current_page(page)
		view = self.GetTabSourceView(page)
		self.goto_line(view, line_number)
		
	def goto_line(self, view, line_number):
		# Belirli bir satıra gitmek için
		buffer = view.get_buffer()
		iter = buffer.get_iter_at_line(line_number - 1)  # Satır numarası 0-index
		view.scroll_to_iter(iter, 0.1, False, 0, 0)
		buffer.place_cursor(iter)

	def check_syntax(self, file, page):
		self.SaveFile()
		self.test_syntax(file, page)
		self.syntax_check_timer = None
		return False

	def CheckFile(self, file):
		os.system("pychecker " + file)
		
	def on_text_changed(self, buffer, file, page):
		# If a previous timer is running, cancel it
		if self.syntax_check_timer is not None:
			GLib.Source.remove(self.syntax_check_timer)
		
		# Set a new timer for 5 seconds
		self.syntax_check_timer = GLib.timeout_add(2000, self.check_syntax, file, page)

	def NewEditorTab(self, filepath):
		scrolled_view = Gtk.ScrolledWindow()
		scrolled_view.set_shadow_type(Gtk.ShadowType.NONE)
		editor_view = GtkSource.View()
		editor_view.get_buffer().begin_not_undoable_action()
		editor_view.get_buffer().connect("highlight-updated", lambda _: self.CheckFile(filepath))
		filepath_lang = filepath
		filepath_lang_array = filepath.rsplit(".", 1)
			
		if len(filepath_lang_array) > 1:
			if filepath_lang_array[1].startswith(tuple(["glade", "cmb", "ui"])):
				filepath_lang = filepath_lang_array[0] + ".xml"
		
		if len(filepath_lang_array) == 1:
			filepath_lang_array.append("dummy_ext")
		langman = GtkSource.LanguageManager()
		editor_view.get_buffer().set_language(langman.guess_language(filepath_lang))
		editor_view.modify_font(Pango.FontDescription("monospace"))
		editor_view.set_show_line_numbers(True)
		with open(filepath, "r", encoding="utf-8") as filecontent:
			editor_view.get_buffer().set_text(filecontent.read())
			filecontent.close()
		scrolled_view.add(editor_view)
		paned = None
		if filepath_lang_array[1].endswith(tuple(["htm", "html"])):
			paned = Gtk.Paned()
			web_view = WebKit2.WebView()
			web_view.set_size_request(128,1)
			paned.pack1(scrolled_view, True, False)
			paned.pack2(web_view, True, True)
			web_view.load_uri("file://" + filepath)

		box:Gtk.Box = Gtk.VBox()
		if paned:
			paned.pack1(scrolled_view, True, False)
			box.pack_start(paned, True, True, 0)
		else:
			box.pack_start(scrolled_view, True, True, 0)

		# Create the status bar
		status_bar = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
		
		search_replace = SearchReplace(editor_view.get_buffer(), editor_view)
		status_bar.pack_start(search_replace, True, True, 0)
		
		cursor_position_label = Gtk.Label("Line: 1, Column: 1  ")
		status_bar.pack_end(cursor_position_label, False, False, 0)

		box.pack_start(Gtk.Separator(), False, False, 0)
		box.pack_end(status_bar, False, False, 0)

		# Connect a signal handler to update cursor position
		def update_cursor_position(arg1, arg2, arg3):
			cursor_iter = editor_view.get_buffer().get_iter_at_mark(editor_view.get_buffer().get_insert())
			line = cursor_iter.get_line() + 1  # Line numbers are 0-based
			col = cursor_iter.get_line_offset() + 1  # Column numbers are 0-based
			cursor_position_label.set_text(f"Line: {line}, Column: {col}  ")

		editor_view.get_buffer().connect("mark-set", update_cursor_position)

		tab_title = Gtk.HBox()
		tab_title.set_vexpand(True)
		tab_title.set_spacing(4)
		tab_close = Gtk.Button()
		tab_close.props.relief = Gtk.ReliefStyle.NONE
		tab_close.set_image(Gtk.Image.new_from_gicon(Gio.ThemedIcon.new("close-symbolic"), Gtk.IconSize.MENU))
		page = self.editor_tabs.append_page(box, tab_title)
		editor_view.get_buffer().connect("changed", self.on_text_changed, filepath, page)
		tab_close.connect("clicked", lambda _: self.editor_tabs.remove(box))
		file_name = str(filepath).rsplit("/", 1)[1]
		file_proj_path = filepath.replace(self.project_dir, "").replace(file_name, "")
		if file_proj_path.startswith("/"):
			file_proj_path = file_proj_path[1:]
		file_proj_path_label = Gtk.Label()
		file_proj_path_label.set_markup("<span color='#c5c5c5'>" + file_proj_path + "</span>" + file_name)
		tab_title.set_center_widget(file_proj_path_label)
		tab_title.pack_end(tab_close, True, True, 0)
		self.editor_tabs.set_tab_detachable(tab_title, True)
		self.editor_tabs.set_tab_reorderable(tab_title, True)
		tab_title.show_all()
		
		completion = editor_view.get_completion()
		extra_paths = ["/Users/urmom/.cache/fakegir/"]
		provider = JediCompletionProvider(extra_paths=extra_paths)
		completion.add_provider(provider)
		
		self.editor_tabs.show_all()
		search_replace.hide()
		self.editor_tabs.next_page()
		editor_view.get_buffer().end_not_undoable_action()
		if self.syntax_check_timer:
			self.clear_warnings()
			GLib.Source.remove(self.syntax_check_timer)
			
		self.syntax_check_timer = None

	def NewEditorImageTab(self, filepath):
		scrolled_view = Gtk.ScrolledWindow()
		scrolled_view.set_overlay_scrolling(True)
		scrolled_view.set_shadow_type(Gtk.ShadowType.NONE)
		image_view = Gtk.Image()
		image_view.set_from_file(filepath)
		scrolled_view.add(image_view)
		box = Gtk.Box()
		box.pack_start(scrolled_view, True, True, 0)
		tab_title = Gtk.HBox()
		tab_title.set_vexpand(True)
		tab_title.set_spacing(4)
		tab_close = Gtk.Button()
		tab_close.props.relief = Gtk.ReliefStyle.NONE
		tab_close.set_image(Gtk.Image.new_from_gicon(Gio.ThemedIcon.new("close-symbolic"), Gtk.IconSize.MENU))
		page = self.editor_tabs.append_page(box, tab_title)
		tab_close.connect("clicked", lambda _: self.editor_tabs.remove(box))
		tab_title.pack_start(Gtk.Label(label=str(filepath).rsplit("/", 1)[1]), True, True, 0)
		tab_title.pack_start(tab_close, True, True, 1)
		self.editor_tabs.set_tab_detachable(tab_title, True)
		self.editor_tabs.set_tab_reorderable(tab_title, True)
		tab_title.show_all()
		self.editor_tabs.show_all()
		self.editor_tabs.next_page()

	def NewEditorTerminal(self, title="Terminal", command=[]):
		self.TerminalCounter += 1
		local_counter = self.TerminalCounter
		terminal_view = Vte.Terminal()
		box = Gtk.Box()
		box.pack_start(terminal_view, True, True, 0)
		self.editor_vte.set_tab_detachable(box, True)
		self.editor_vte.set_tab_reorderable(box, True)
		tab_title = Gtk.HBox()
		tab_title.set_vexpand(True)
		tab_title.set_spacing(4)
		tab_close = Gtk.Button()
		tab_close.props.relief = Gtk.ReliefStyle.NONE
		tab_close.set_image(Gtk.Image.new_from_gicon(Gio.ThemedIcon.new("close-symbolic"), Gtk.IconSize.MENU))
		page = self.editor_vte.append_page(box, tab_title)
		tab_close.connect("clicked", lambda _: self.editor_vte.remove(box))
		tab_title.pack_start(Gtk.Label(title + " " + str(self.TerminalCounter)), True, True, 0)
		tab_title.pack_start(tab_close, True, True, 1)
		tab_title.show_all()
		
		if command == []:
			command = ["bash"]
			
		terminal_view.connect("child-exited", lambda terminal, status: self.editor_vte.remove(box))
			
		terminal_view.spawn_sync(
			Vte.PtyFlags.DEFAULT,
			self.project_dir,
			command,
			[],
			0,
			None,
			None,
			None
		)
		self.editor_vte.show_all()
		self.editor_vte.next_page()

	def RefreshProjectTree(self):
		self.project_tree_model.clear()
		self.project_dir_path_dict = {
			"0:0:0": self.project_dir + "/project_props/apt_requirements/amd64",
			"0:0:1": self.project_dir + "/project_props/apt_requirements/arm64",
			"0:1": self.project_dir + "/project_props/bundle_store/meta.ninf",
		}
		startpath = self.project_dir
	
		# Create root nodes
		project_props = self.project_tree_model.append(None, ["Project Properties"])
		apt_req = self.project_tree_model.append(project_props, ["Requirements from LegacyPkg (apt)"])
		apt_x86 = self.project_tree_model.append(apt_req, ["64-bit Intel/AMD x86 (PC, Mac, etc.)"])
		apt_arm = self.project_tree_model.append(apt_req, ["64-bit Advanced Risc Machine (Raspberry Pi, NebiPad, etc.)"])
		bundle_store = self.project_tree_model.append(project_props, ["Bundle Store Information & Assets"])
		project_builds = self.project_tree_model.append(None, ["Project Bundles"])
		
		# Add project build directories
		try:
			self.add_files_to_tree(project_builds, startpath + "/output_pkg")
		except Exception as e:
			print(f"Error adding build directories: {e}")
		
		# Add project files
		project_files = self.project_tree_model.append(None, ["Project Files"])
		self.add_files_to_tree(project_files, self.project_dir, "2")
		
		# Restore expanded paths
		self.RestoreExpandedPaths()

	def add_files_to_tree(self, parent_node, path, parent_path_key=""):
		"""Adds files and directories from the given path to the tree under parent_node.
		   Updates the project_dir_path_dict with tree paths and file paths, excluding dotfiles."""
		if os.path.isdir(path):
			index = 0
			for item in sorted(os.listdir(path)):
				# Skip dotfiles
				if item.startswith('.') or item in EDITOR_TREE_BLACKLIST:
					continue
				
				item_path = os.path.join(path, item)
				tree_path_key = f"{parent_path_key}:{index}"
				
				try:
					print("Adding to tree:", item_path)
				except UnicodeEncodeError:
					print(f"Failed to encode item_path: {item_path.encode('utf-8', 'replace')}")
				
				try:
					if os.path.isdir(item_path):
						node = self.project_tree_model.append(parent_node, [item])
						self.add_files_to_tree(node, item_path, tree_path_key)
					else:
						self.project_tree_model.append(parent_node, [item])
						self.project_dir_path_dict[tree_path_key] = item_path
				except UnicodeEncodeError:
					print(f"Failed to process item_path: {item_path.encode('utf-8', 'replace')}")
				
				index += 1
		else:
			# If path is a file, add it directly if it's not a dotfile
			if not os.path.basename(path).startswith('.') or not os.path.basename(path) in EDITOR_TREE_BLACKLIST:
				self.project_tree_model.append(parent_node, [os.path.basename(path)])
				self.project_dir_path_dict[parent_path_key] = path

	def CloseProject(self):
		if self.PARENT:
			self.SaveExpandedPaths()
			self.returns_to_main_menu = True
			self.editor_window.close()
			self.PARENT.MainWindow.show_all()

	def QuitEditor(self):
		self.SaveExpandedPaths()
		if self.returns_to_main_menu != True:
			Gtk.main_quit(0)

if __name__ == "__main__":
	Editor(sys.argv[1])
	Gtk.main()
