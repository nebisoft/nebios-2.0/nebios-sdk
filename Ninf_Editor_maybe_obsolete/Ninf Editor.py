import gi
gi.require_version("Gtk", "3.0")
from gi.overrides import Gtk, GObject
from gi.repository import Gtk, GObject
from os.path import abspath, dirname, join

WHERE_AM_I = abspath(dirname(__file__))

class MyApp(object):

    def __init__(self):
        self.builder = Gtk.Builder()
        self.glade_file = join(WHERE_AM_I, 'ui/app.glade')
        #GObject.type_register(GtkSource.View)
        self.builder.add_from_file(self.glade_file)
        self.window = self.builder.get_object("window1")

        self.appName = self.builder.get_object("appName")
        self.appVer = self.builder.get_object("appVer")
        self.genericName = self.builder.get_object("genericName")
        self.comment = self.builder.get_object("comment")
        self.bsDesc = self.builder.get_object("bsDesc")
        self.keywordTags = self.builder.get_object("keywordTags")
        self.categoryTags = self.builder.get_object("categoryTags")
        self.mimeTags = self.builder.get_object("mimeTags")
        self.authorName = self.builder.get_object("authorName")
        self.maintainerName = self.builder.get_object("maintainerName")
        self.webPage = self.builder.get_object("webPage")
        self.appSize = self.builder.get_object("appSize")
        self.appLicense = self.builder.get_object("appLicense")

        self.builder.get_object("saveBtn").connect("clicked", self.saveDlg)

        self.window.show_all()
        self.window.connect("destroy", exit)

    def saveDlg(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Save Metadata", parent=self.window, action=Gtk.FileChooserAction.SAVE
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
            self.saveFile(dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
            print("Cancel clicked")

        dialog.destroy()

    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("NebiOS Application Info (*.ninf)")
        filter_text.add_pattern("*.ninf")
        dialog.add_filter(filter_text)

    def getText(self, textview):
        buffer = textview.get_buffer()
        startIter, endIter = buffer.get_bounds()
        text = buffer.get_text(startIter, endIter, False)
        return text

    def saveFile(self, location):
        appName = str(self.appName.get_text())
        appVer = str(self.appVer.get_text())
        genericName = str(self.genericName.get_text())
        comment = self.getText(self.comment)
        bsDesc = self.getText(self.bsDesc)
        keywords = str(self.keywordTags.get_text())
        cat = str(self.categoryTags.get_text())
        mime = str(self.mimeTags.get_text())
        author = str(self.authorName.get_text())
        publisher = str(self.maintainerName.get_text())
        web = str(self.webPage.get_text())
        size = str(self.appSize.get_text())
        license = str(self.appLicense.get_text())

        fullMetadata = """ Name=""" + appName + """
Version="""+appVer+"""
GenericName="""+genericName+"""
Comment="""+comment+"""
StoreDescription='""" + bsDesc + """'
Keywords="""+keywords+"""
Categories="""+cat+"""
MimeType="""+mime+"""
Author='"""+author+"""'
Maintainer='"""+publisher+"""'
Webpage='"""+web+"""'
Platform='Unix (NebiOS)'
License='"""+license+"""'
Size='"""+size+"'"

        f = open(location, "w")
        f.write(fullMetadata)
        f.close()



if __name__ == '__main__':
    try:
        gui = MyApp()
        Gtk.main()
    except KeyboardInterrupt:
        pass
