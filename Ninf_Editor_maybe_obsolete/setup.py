from cx_Freeze import setup, Executable
buildOptions = dict(packages = [], excludes = [])

import sys
base = 'Win32GUI' if sys.platform =='win32' else None

executables = [
    Executable('Ninf Editor.py',
    )
              ]

setup(
    name='Ninf Editor',
    version = '2.0',
    description = 'Ninf Editor',
    options = dict(build_exe = buildOptions),
    executables = executables
    )
