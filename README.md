# NebiOS SDK (Software Development Kit)

This repo contains all utilities for developing (& publising your apps on Bundle Store) on NebiOS Platform.

## Nedev

Nedev is a IDE written in mostly Python from scratch. Mostly used for developing applications for NebiOS (both 64-bit ARM & 64-bit x86 - Intel and AMD) but also includes basic HTML/CSS/JS editor with preview & basic web server (requires installation of PHP (Optional) & Apache & MySQL/SQLite/PostgreSQL (Optional) via .nspkg format).

### Widget Editor

This utility allows create Widgets specific to NebiDE (Nebi Desktop Environment) Wallpaper Engine.

### Crosinset Emulator (CROss-platform INStruction SET Emulator)

This QEMU-based emulator (Wiht improved emulator performance) allows test your applications on ARM-based (emulates devices like Raspberry Pi 4 & NebiSoft's Padnther Project) NebiOS environments.

### MaintainHub

This utility allows manage your applications on the official instance of the Bundle Store. NebiSoft Developer Account (you can made this for $5 each app or free for personal/startup/non-profit use) required.

## Exec2napp

This utility converts your Linux applications (If you're porting someone's application, please port from tarball version. AppImage is OK but making an bundle that installs an DEB package is not reccommended and we will prevent usage of the apt/dpkg on inside of the napp container). Works in most Linux distributions. makeself is required to use right now (We will fork makeself and will make napp bundles more efficient to work).

## Ninf Editor (obsolete right now)

A GTK frontend for creating .napp metainfo from zero.
